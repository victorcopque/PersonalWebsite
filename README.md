PersonalWebsite
===============

Criação do meu site pessoal.

### Características

> - Projetos: Descrição e linha histórica dos meus projetos 
> - Histórico de cargos assumidos em organizações[assim como no Linkedin]
> - Blog: postagens sobre conteúdos interessantes para mim[como um blog]
> - Contato: formulário para que o visitante possa entrar em contato comigo através do email profissional
> - Site Responsivo
> - Utilização de SASS
